## Coinbase Order Book

Implemented with Spring Boot 2.4.5, Spring WebFlux, Java 8 and Maven.
Unit tests are under the /src/test package.

# Build
To build the package run:
```shell
mvn clean package
```

# Run
To run the application run:
```shell
java -jar target/coinbase-order-book.jar CCY-CCY
# where CCY-CCY can be any valid Coinbase API product ID like ETH-USD or ETH-BTC.
```
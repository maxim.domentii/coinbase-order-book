package com.example.coinbaseorderbook.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class L2Response implements Serializable {

    private static final long serialVersionUID = -160201569889182286L;

    private String type;
    private List<Channel> channels;
    @JsonProperty("product_id")
    private String productId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss[.SSSSSS]X")
    private LocalDateTime time;
    private List<String[]> bids;
    private List<String[]> asks;
    private List<String[]> changes;

    public L2Response() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public List<String[]> getBids() {
        return bids;
    }

    public void setBids(List<String[]> bids) {
        this.bids = bids;
    }

    public List<String[]> getAsks() {
        return asks;
    }

    public void setAsks(List<String[]> asks) {
        this.asks = asks;
    }

    public List<String[]> getChanges() {
        return changes;
    }

    public void setChanges(List<String[]> changes) {
        this.changes = changes;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Channel implements Serializable {
        private static final long serialVersionUID = 9001131033260062756L;

        private String name;
        @JsonProperty("product_ids")
        private List<String> productIds;

        public Channel() {

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getProductIds() {
            return productIds;
        }

        public void setProductIds(List<String> productIds) {
            this.productIds = productIds;
        }

        @Override
        public String toString() {
            return "Channel{" +
                    "name='" + name + '\'' +
                    ", productIds=" + productIds +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "L2Response{" +
                "type='" + type + '\'' +
                ", channels=" + channels +
                ", productId='" + productId + '\'' +
                ", time=" + time +
                ", bids=" + bids +
                ", asks=" + asks +
                ", changes=" + changes +
                '}';
    }
}

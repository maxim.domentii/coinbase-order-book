package com.example.coinbaseorderbook.service;

public interface IWebSocketClient {

    void run(String productId);
}

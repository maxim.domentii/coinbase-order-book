package com.example.coinbaseorderbook.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

public class SubscribeRequest {

    private String type;
    private List<String> channels;
    @JsonProperty("product_ids")
    private List<String> productIds;

    public SubscribeRequest(String productId){
        this.type = "subscribe";
        this.channels = Collections.singletonList("level2");
        this.productIds = Collections.singletonList(productId);
    }

    public String getType() {
        return type;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public List<String> getChannels() {
        return channels;
    }
}

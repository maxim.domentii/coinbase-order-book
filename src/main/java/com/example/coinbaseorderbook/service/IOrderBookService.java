package com.example.coinbaseorderbook.service;

import com.example.coinbaseorderbook.data.BidAskEntry;
import com.example.coinbaseorderbook.data.L2Response;

import java.math.BigDecimal;
import java.util.Map;

public interface IOrderBookService {

    L2Response processL2Response(L2Response response);
    Map<BigDecimal, BidAskEntry> getBids();
    Map<BigDecimal, BidAskEntry> getAsks();
    String getProductId();
}

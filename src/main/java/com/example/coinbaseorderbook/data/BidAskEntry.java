package com.example.coinbaseorderbook.data;

import java.math.BigDecimal;

public class BidAskEntry {
    private BigDecimal price;
    private BigDecimal size;

    public BidAskEntry(BigDecimal price, BigDecimal size) {
        this.price = price;
        this.size = size;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getSize() {
        return size;
    }
}

package com.example.coinbaseorderbook.service;

import com.example.coinbaseorderbook.data.BidAskEntry;
import com.example.coinbaseorderbook.data.L2Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

@Service
public class OrderBookService implements IOrderBookService {

    private static final Logger log = LoggerFactory.getLogger(OrderBookService.class);

    private static final BigDecimal ZERO = BigDecimal.ZERO;
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0,000,000.0000000000");
    protected static final String SNAPSHOT = "snapshot";
    protected static final String L_2_UPDATE = "l2update";
    protected static final String SUBSCRIPTIONS = "subscriptions";
    protected static final String SELL = "sell";
    protected static final String BUY = "buy";

    private Map<BigDecimal, BidAskEntry> bids;
    private Map<BigDecimal, BidAskEntry> asks;
    private String productId = "";

    public OrderBookService() {
        this.bids = new TreeMap<>(Collections.reverseOrder());
        this.asks = new TreeMap<>();
    }

    @Override
    public Map<BigDecimal, BidAskEntry> getBids() {
        return bids;
    }

    @Override
    public Map<BigDecimal, BidAskEntry> getAsks() {
        return asks;
    }

    @Override
    public String getProductId() {
        return productId;
    }

    @Override
    public L2Response processL2Response(L2Response response) {
        switch (response.getType()){
            case SNAPSHOT:
                processSnapshotResponse(response);
                logOrderBook();
                break;
            case L_2_UPDATE:
                processL2UpdateResponse(response);
                logOrderBook();
                break;
            case SUBSCRIPTIONS:
                log.info("Subscription response received {}", response);
                if (response.getChannels() == null || response.getChannels().size() == 0
                        || response.getChannels().get(0).getProductIds() == null
                        || response.getChannels().get(0).getProductIds().size() == 0){
                    throw new RuntimeException("Invalid productId! Subscribe with a proper one.");
                } else {
                    this.productId = response.getChannels().get(0).getProductIds().get(0);
                }
                break;
            default:
                log.error("Unknown response type received! Ignored for processing.");
        }

        return response;
    }

    private void logOrderBook() {
        List<BidAskEntry> firstTenAsks = asks.size() >= 10 ? new ArrayList<>(asks.values()).subList(0, 10)
                : new ArrayList<>(asks.values()).subList(0, asks.size());
        List<BidAskEntry> firstTenBids = bids.size() >= 10 ? new ArrayList<>(bids.values()).subList(0, 10)
                : new ArrayList<>(bids.values()).subList(0, bids.size());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n").append("======================================================================================");
        stringBuilder.append("\n").append("ORDER BOOK for ").append(productId);
        stringBuilder.append("\n").append("======================================================================================");
        stringBuilder.append("\n").append("Asks                                      || Bids");
        stringBuilder.append("\n").append("--------------------------------------------------------------------------------------");
        stringBuilder.append("\n").append("| Size               | Price              || Price              | Size               |");
        stringBuilder.append("\n").append("--------------------------------------------------------------------------------------");
        int minTopBidsAsksSize = Math.min(Math.min(asks.size(), bids.size()), 10);
        for(int i = 0; i< minTopBidsAsksSize; i++){
            String askPrice = DECIMAL_FORMAT.format(firstTenAsks.get(i).getPrice());
            String askSize = DECIMAL_FORMAT.format(firstTenAsks.get(i).getSize());
            String bidPrice = DECIMAL_FORMAT.format(firstTenBids.get(i).getPrice());
            String bidSize = DECIMAL_FORMAT.format(firstTenBids.get(i).getSize());
            stringBuilder.append("\n").append("|").append(askSize).append("|").append(askPrice).append("||");
            stringBuilder.append(bidPrice).append("|").append(bidSize).append("|");
        }
        stringBuilder.append("\n").append("--------------------------------------------------------------------------------------");

        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.print(stringBuilder.toString());
    }

    private void processL2UpdateResponse(L2Response response) {
        for (String[] change : response.getChanges()){
            if (change.length != 3 || !SELL.equals(change[0]) && !BUY.equals(change[0])){
                log.error("Invalid l2update change entry {}. Ignoring it...", Arrays.toString(change));
            } else {
                if (SELL.equals(change[0])) {
                    processBidAskEntry(asks, mapResponseEntryToBidAskEntry(change));
                } else {
                    processBidAskEntry(bids, mapResponseEntryToBidAskEntry(change));
                }
            }
        }
    }

    private void processBidAskEntry(Map<BigDecimal, BidAskEntry> bidsOrAsksMap, BidAskEntry entry){
        if (ZERO.compareTo(entry.getSize()) == 0 && bidsOrAsksMap.containsKey(entry.getPrice())) {
            bidsOrAsksMap.remove(entry.getPrice());
        } else {
            bidsOrAsksMap.put(entry.getPrice(), entry);
        }
    }

    private void processSnapshotResponse(L2Response response) {
        for (String[] ask : response.getAsks()){
            if (ask.length != 2){
                log.error("Invalid snapshot ask entry {}. Ignoring it...", Arrays.toString(ask));
            } else {
                BidAskEntry bidAskEntry = mapResponseEntryToBidAskEntry(ask);
                asks.put(bidAskEntry.getPrice(), bidAskEntry);
            }
        }
        for (String[] bid : response.getBids()){
            if (bid.length != 2){
                log.error("Invalid snapshot bid entry {}. Ignoring it...", Arrays.toString(bid));
            } else {
                BidAskEntry bidAskEntry = mapResponseEntryToBidAskEntry(bid);
                bids.put(bidAskEntry.getPrice(), bidAskEntry);
            }
        }
    }

    private BidAskEntry mapResponseEntryToBidAskEntry(String[] responseEntry) {
        if (responseEntry.length == 2){
            return new BidAskEntry(new BigDecimal(responseEntry[0]), new BigDecimal(responseEntry[1]));
        } else if (responseEntry.length == 3) {
            return new BidAskEntry(new BigDecimal(responseEntry[1]), new BigDecimal(responseEntry[2]));
        } else {
            log.error("Invalid response price-size entry!");
            throw new RuntimeException("Invalid response price-size entry!");
        }
    }
}

package com.example.coinbaseorderbook.service;

import com.example.coinbaseorderbook.data.L2Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class OrderBookServiceTest {

    private IOrderBookService orderBookService;

    @BeforeEach
    void setUp() {
        orderBookService = new OrderBookService();
    }

    @Test
    void testProcessL2ResponseWhenSubscriptionsResponseHasNoProductIdThenThrowsError() {
        // given
        L2Response response = new L2Response();
        response.setType(OrderBookService.SUBSCRIPTIONS);
        L2Response.Channel channel = new L2Response.Channel();
        channel.setProductIds(Collections.emptyList());
        response.setChannels(Collections.singletonList(channel));

        try {
            // when
            orderBookService.processL2Response(response);
            fail("Has to throw exception!");
        } catch (RuntimeException e){
            // then
            assertEquals("Invalid productId! Subscribe with a proper one.", e.getMessage());
        }
    }

    @Test
    void testProcessL2ResponseWhenSubscriptionsResponseHasNoChannelsThenThrowsError() {
        // given
        L2Response response = new L2Response();
        response.setType(OrderBookService.SUBSCRIPTIONS);
        response.setChannels(Collections.emptyList());

        try {
            // when
            orderBookService.processL2Response(response);
            fail("Has to throw exception!");
        } catch (RuntimeException e){
            // then
            assertEquals("Invalid productId! Subscribe with a proper one.", e.getMessage());
        }
    }

    @Test
    void testProcessL2ResponseWhenSubscriptionsResponseThenSetProductId() {
        // given
        L2Response response = new L2Response();
        response.setType(OrderBookService.SUBSCRIPTIONS);
        L2Response.Channel channel = new L2Response.Channel();
        String productId = "ETH-USD";
        channel.setProductIds(Collections.singletonList(productId));
        response.setChannels(Collections.singletonList(channel));

        // when
        orderBookService.processL2Response(response);

        // then
        assertEquals(productId, orderBookService.getProductId());
    }

    @Test
    void testProcessL2ResponseWhenSnapshotResponseThenSetParseAsksAndBids() {
        // given
        L2Response response = new L2Response();
        response.setType(OrderBookService.SNAPSHOT);
        String askPrice = "2101.01";
        String askSize = "0.01";
        response.setAsks(Arrays.asList(new String[]{askPrice, askSize}, new String[]{"something", "2101.02", "0.009"}));

        String bidPrice = "2101.02";
        String bidSize = "0.009";
        response.setBids(Arrays.asList(new String[]{bidPrice, bidSize}, new String[]{"something", "2101.02", "0.009"}));

        // when
        orderBookService.processL2Response(response);

        // then
        assertEquals(1, orderBookService.getAsks().size());
        assertEquals(new BigDecimal(askPrice), orderBookService.getAsks().get(new BigDecimal(askPrice)).getPrice());
        assertEquals(new BigDecimal(askSize), orderBookService.getAsks().get(new BigDecimal(askPrice)).getSize());

        assertEquals(1, orderBookService.getBids().size());
        assertEquals(new BigDecimal(bidPrice), orderBookService.getBids().get(new BigDecimal(bidPrice)).getPrice());
        assertEquals(new BigDecimal(bidSize), orderBookService.getBids().get(new BigDecimal(bidPrice)).getSize());
    }

    @Test
    void testProcessL2ResponseWhenUpdateResponseThenSetParseAsksAndBids() {
        // given
        L2Response response = new L2Response();
        response.setType(OrderBookService.L_2_UPDATE);
        String askPrice = "2101.01";
        String askSize = "0.01";
        String bidPrice = "2101.02";
        String bidSize = "0.009";
        response.setChanges(Arrays.asList(
                new String[]{OrderBookService.SELL, askPrice, askSize},
                new String[]{OrderBookService.BUY, bidPrice, bidSize},
                new String[]{"2101.02", "0.009"}));


        // when
        orderBookService.processL2Response(response);

        // then
        assertEquals(1, orderBookService.getAsks().size());
        assertEquals(new BigDecimal(askPrice), orderBookService.getAsks().get(new BigDecimal(askPrice)).getPrice());
        assertEquals(new BigDecimal(askSize), orderBookService.getAsks().get(new BigDecimal(askPrice)).getSize());

        assertEquals(1, orderBookService.getBids().size());
        assertEquals(new BigDecimal(bidPrice), orderBookService.getBids().get(new BigDecimal(bidPrice)).getPrice());
        assertEquals(new BigDecimal(bidSize), orderBookService.getBids().get(new BigDecimal(bidPrice)).getSize());
    }
}
package com.example.coinbaseorderbook.service;

import com.example.coinbaseorderbook.data.L2Response;
import com.example.coinbaseorderbook.data.SubscribeRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.WebsocketClientSpec;

import java.net.URI;

@Service
public class ReactorWebSocketClient implements IWebSocketClient {

    private static final Logger log = LoggerFactory.getLogger(ReactorWebSocketClient.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    private WebSocketClient client;
    private String url;
    private IOrderBookService orderBookService;

    public ReactorWebSocketClient(@Value("${websocket.feed.url}") String url,
                                  @Autowired IOrderBookService orderBookService) {
        this.url = url;
        this.orderBookService = orderBookService;
        this.client = new ReactorNettyWebSocketClient(HttpClient.create(),
                WebsocketClientSpec.builder().maxFramePayloadLength(524288)); /* 512KB */
    }

    @Override
    public void run(String productId) {
        client.execute(
                URI.create(url),
                session -> {
                    String requestAsString = getSubscribeRequestAsString(productId);
                    log.info("Subscribing with request {}", requestAsString);
                    return session.send(
                            Mono.just(session.textMessage(requestAsString)))
                            .thenMany(session.receive()
                                    .map(WebSocketMessage::getPayloadAsText)
                                    .map(this::getResponsePayloadAsObject)
                                    .map(orderBookService::processL2Response))
                            .then();
                }
        ).block();
    }

    private String getSubscribeRequestAsString(String productId) {
        SubscribeRequest subscribeRequest = new SubscribeRequest(productId);
        try {
            return objectMapper.writeValueAsString(subscribeRequest);
        } catch (JsonProcessingException e) {
            log.error("Request mapping failed!", e);
            throw new RuntimeException("Request mapping failed!", e);
        }
    }

    private L2Response getResponsePayloadAsObject(String payload){
        try {
            return objectMapper.readValue(payload, L2Response.class);
        } catch (JsonProcessingException e) {
            log.error("Response mapping failed!", e);
            throw new RuntimeException("Response mapping failed!", e);
        }
    }
}

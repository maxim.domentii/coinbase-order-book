package com.example.coinbaseorderbook;

import com.example.coinbaseorderbook.service.IWebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CommandLiner implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CommandLiner.class);

    private IWebSocketClient webSocketClient;

    public CommandLiner(@Autowired IWebSocketClient webSocketClient) {
        this.webSocketClient = webSocketClient;
    }

    @Override
    public void run(String... args) {
        if (args == null || args.length == 0){
            log.error("Usage: java -jar coinbase-order-book.jar CCY-CCY");
            System.exit(1);
        }

        webSocketClient.run(args[0]);
    }
}
